<div class="product-tabs__pane" id="tab-feedbacks">
    <div class="reviews-view">
        <div class="reviews-view__list">
            <h3 class="reviews-view__header">Отзывы покупателей</h3>
            <div class="reviews-list">
                <ol class="reviews-list__content">
                    @if($product->feedbacks()->where('confirm',true)->count() <= 0)
                        <div style="text-align: center; font-size: 18px;">
                            <p>ОТЗЫВОВ НЕТ</p>
                        </div>
                    @endif
                    @foreach($product->feedbacks()->where('confirm',true)->get() as $feedback)
                        <li class="reviews-list__item">
                            <div class="review">
                                <div class="review__avatar"><img src="{{asset('site/custom/images/feedbacks/avatars/'.$feedback->avatar)}}" alt=""></div>
                                <div class="review__content">
                                    <div class="review__author">{{$feedback->name}}</div>
                                    <div class="review__rating">
                                        <div class="rating">
                                            <div class="rating__body">
                                                @include('site.pages.product.feedbacks.star_active')
                                                @if ($feedback->rating >= 2)
                                                    @include('site.pages.product.feedbacks.star_active')
                                                @else
                                                    @include('site.pages.product.feedbacks.star_unactive')
                                                @endif
                                                @if ($feedback->rating >= 3)
                                                    @include('site.pages.product.feedbacks.star_active')
                                                @else
                                                    @include('site.pages.product.feedbacks.star_unactive')
                                                @endif
                                                @if ($feedback->rating >= 4)
                                                    @include('site.pages.product.feedbacks.star_active')
                                                @else
                                                    @include('site.pages.product.feedbacks.star_unactive')
                                                @endif
                                                @if ($feedback->rating >= 5)
                                                    @include('site.pages.product.feedbacks.star_active')
                                                @else
                                                    @include('site.pages.product.feedbacks.star_unactive')
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="review__text">{{$feedback->content}}</div>
                                    <div class="review__date">{{$feedback->created_at->format('d.m.Y')}}</div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ol>
            </div>
        </div>

        <form id="form__feedback" method="post" action="{{route('create_feedback',['product_id'=>$product->id])}}" class="reviews-view__form" style="border-top: none; padding-top: 0;">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @csrf
            <h3 class="reviews-view__header">Оставить отзыв</h3>
            <div class="row">
                <div class="col-12 col-lg-12 col-xl-12">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="review-stars">Ваша оценка</label>
                            <select name="rating" id="review-stars" class="form-control">
                                <option value="5">5 Отлично</option>
                                <option value="4">4 Хорошо</option>
                                <option value="3">3 Удовл.</option>
                                <option value="2">2 Неудовл.</option>
                                <option value="1">1 Плохо</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="review-author">Ваше имя</label>
                            <input name="name" type="text" class="form-control" id="review-author" placeholder="Введите имя.." value="{{old('name')}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="review-email">Email</label>
                            <input name="email" type="email" class="form-control" id="review-email" placeholder="Email.." value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="review-text">Текст</label>
                        <textarea name="text" class="form-control" id="review-text" rows="6">{{old('text')}}</textarea>
                    </div>
                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-lg">Отправить отзыв</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

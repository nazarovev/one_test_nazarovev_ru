@extends('site.layouts.app')

@section('content')
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Корзина</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Корзина
                    @if(Illuminate\Support\Facades\Route::is('showcase_category'))
                        - {{$category->title}}
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="cart block">
        <div class="container">
            <table class="cart__table cart-table">
                <thead class="cart-table__head">
                <tr class="cart-table__row">
                    <th class="cart-table__column cart-table__column--image"></th>
                    <th class="cart-table__column cart-table__column--product">Товар</th>
                    <th class="cart-table__column cart-table__column--price">Цена</th>
                    <th class="cart-table__column cart-table__column--quantity">Кол-во</th>
                    <th class="cart-table__column cart-table__column--total">Сумма</th>
                    <th class="cart-table__column cart-table__column--remove"></th>
                </tr>
                </thead>
                <tbody class="cart-table__body">
                @foreach($cart['products'] as $item)
                    <tr class="cart-table__row">
                        <td class="cart-table__column cart-table__column--image">
                            <div class="product-image">
                                <a href="" class="product-image__body">
                                    <img class="product-image__img"
                                         src="{{asset('site/custom/images/products/'.$item['product']['image'])}}"
                                         alt="">
                                </a>
                            </div>
                        </td>
                        <td class="cart-table__column cart-table__column--product">
                            <a href="" class="cart-table__product-name">{{$item['product']['title']}}</a>
                        </td>
                        <td class="cart-table__column cart-table__column--price"
                            data-title="Price">{{number_format($item['product']['price'],2,',',' ')}} р.
                        </td>
                        <td class="cart-table__column cart-table__column--quantity" data-title="Quantity">
                            <div class="input-number">
                                <input data-product_id="{{$item['product']['id']}}"
                                       class="form-control input-number__input" type="number" min="1"
                                       value="{{$item['quantity']}}">
                                <div class="input-number__add"></div>
                                <div class="input-number__sub"></div>
                            </div>
                        </td>
                        <td class="cart-table__column cart-table__column--total"
                            data-title="Total">{{number_format($item['product']['price'] * $item['quantity'],2,',',' ')}}
                            р.
                        </td>
                        <td class="cart-table__column cart-table__column--remove">
                            <button type="button" class="btn btn-light btn-sm btn-svg-icon btn__del_product_from_cart"
                                    data-product_id="{{$item['product']['id']}}">
                                <svg width="12px" height="12px">
                                    <use xlink:href="{{asset('site/images/sprite.svg#cross-12')}}"></use>
                                </svg>
                            </button>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            <div class="row justify-content-end pt-5">
                <div class="col-12 col-md-6 col-lg-6 col-xl-7">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title"></h3>
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input id="name" name="name" type="text" class="form-control" placeholder="Введите имя..">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" name="email" type="text" class="form-control" placeholder="Введите email..">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-6 col-xl-5">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Итого</h3>
                            <table class="cart__totals">
                                <tfoot class="cart__totals-footer">
                                <tr>
                                    <th>Сумма</th>
                                    <td>{{number_format($cart['total'],2,',',' ')}} р.</td>
                                </tr>
                                </tfoot>
                            </table>
                            <button class="btn btn-primary btn-xl btn-block cart__checkout-button">
                                Сделать заказ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var cart = {{ \Illuminate\Support\Js::from($cart) }};
        console.log(cart);
    </script>

    <script>
        $('.input-number__add').click(function (event) {
            let quantity = $(this).siblings('.input-number__input').val();
            let product_id = $(this).siblings('.input-number__input').data('product_id');
            $.ajax({
                type: "POST",
                url: "{{route('quantity_to_cart')}}",
                data: {
                    _token: secure_token,
                    product_id: product_id,
                    quantity: quantity,
                },
                success: function (data) {
                    // alert(JSON.stringify(data));
                    location.reload();
                },
                dataType: "json",
            });
        });

        $('.input-number__sub').click(function (event) {
            let quantity = $(this).siblings('.input-number__input').val();
            let product_id = $(this).siblings('.input-number__input').data('product_id');
            $.ajax({
                type: "POST",
                url: "{{route('quantity_to_cart')}}",
                data: {
                    _token: secure_token,
                    product_id: product_id,
                    quantity: quantity,
                },
                success: function (data) {
                    // alert(JSON.stringify(data));
                    location.reload();
                },
                dataType: "json",
            });
        });

        $('.btn__del_product_from_cart').click(function (event) {
            let product_id = $(this).data('product_id');

            if (confirm('Удалить товар из корзины?')) {

                $.ajax({
                    type: "POST",
                    url: "{{route('del_from_cart')}}",
                    data: {
                        _token: secure_token,
                        product_id: product_id,
                    },
                    success: function (data) {
                        // alert(JSON.stringify(data));
                        location.reload();
                    },
                    dataType: "json",
                });
            }
        });

        $('.cart__checkout-button').click(function (event) {
            let name = $('#name').val();
            alert(name);
            let email = $('#email').val();
            alert(email);
            if (confirm('Вы уверены, что хотите сделать заказ?')) {
                $.ajax({
                    type: "POST",
                    url: "{{route('checkout')}}",
                    data: {
                        _token: secure_token,
                        cart: cart,
                        name: name,
                        email: email,
                    },
                    success: function (data) {
                        // alert(JSON.stringify(data));
                        location.reload();
                    },
                    dataType: "json",
                });
            }

        });
    </script>
@endpush

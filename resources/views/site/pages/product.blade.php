@extends('site.layouts.app')

@section('content')
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{route('showcase_category',['category_id'=>$product->category_id])}}">{{$product->category->title}}</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{$product->title}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="product product--layout--standard" data-layout="standard">
                <div class="product__content">
                    <!-- .product__gallery -->
                    <div class="product__gallery">
                        <div class="product-gallery">
                            <div class="product-gallery__featured">
                                <div class="owl-carousel" id="product-image">
                                    <div class="product-image product-image--location--gallery">
                                        <a href="{{asset('site/custom/images/products/'.$product->image)}}" data-width="700" data-height="700" class="product-image__body" target="_blank">
                                            <img class="product-image__img" src="{{asset('site/custom/images/products/'.$product->image)}}" alt="">
                                        </a>
                                    </div>
                                    @foreach($product->images as $image)
                                        <div class="product-image product-image--location--gallery">
                                            <a href="{{asset('site/custom/images/products/'.$image->filename)}}" data-width="700" data-height="700" class="product-image__body" target="_blank">
                                                <img class="product-image__img" src="{{asset('site/custom/images/products/'.$image->filename)}}" alt="">
                                            </a>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                            <div class="product-gallery__carousel">
                                <div class="owl-carousel" id="product-carousel">
                                    <a href="{{asset('site/custom/images/products/'.$product->image)}}" class="product-image product-gallery__carousel-item">
                                        <div class="product-image__body">
                                            <img class="product-image__img product-gallery__carousel-image" src="{{asset('site/custom/images/products/'.$product->image)}}" alt="">
                                        </div>
                                    </a>
                                    @foreach($product->images as $image)
                                        <a href="{{asset('site/custom/images/products/'.$image->filename)}}" class="product-image product-gallery__carousel-item">
                                            <div class="product-image__body">
                                                <img class="product-image__img product-gallery__carousel-image" src="{{asset('site/custom/images/products/'.$image->filename)}}" alt="">
                                            </div>
                                        </a>
                                    @endforeach

{{--                                    <a href="{{asset('site/images/products/product-16-2.jpg')}}" class="product-image product-gallery__carousel-item">--}}
{{--                                        <div class="product-image__body">--}}
{{--                                            <img class="product-image__img product-gallery__carousel-image" src="{{asset('site/images/products/product-16-2.jpg')}}" alt="">--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                    <a href="{{asset('site/images/products/product-16-3.jpg')}}" class="product-image product-gallery__carousel-item">--}}
{{--                                        <div class="product-image__body">--}}
{{--                                            <img class="product-image__img product-gallery__carousel-image" src="{{asset('site/images/products/product-16-3.jpg')}}" alt="">--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                    <a href="{{asset('site/images/products/product-16-4.jpg')}}" class="product-image product-gallery__carousel-item">--}}
{{--                                        <div class="product-image__body">--}}
{{--                                            <img class="product-image__img product-gallery__carousel-image" src="{{asset('site/images/products/product-16-4.jpg')}}" alt="">--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .product__gallery / end -->
                    <!-- .product__info -->
                    <div class="product__info">
                        <div class="product__wishlist-compare">
                            <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Wishlist">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{asset('site/images/sprite.svg#wishlist-16')}}"></use>
                                </svg>
                            </button>
                            <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Compare">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{asset('site/images/sprite.svg#compare-16')}}"></use>
                                </svg>
                            </button>
                        </div>
                        <h1 class="product__name">{{$product->title}}</h1>
                        <div class="product__rating">
                            <div class="product__rating-stars">
                                <div class="rating">
                                    <div class="rating__body">
                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                            <g class="rating__fill">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                            </g>
                                            <g class="rating__stroke">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                            </g>
                                        </svg>
                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                            <div class="rating__fill">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                            <div class="rating__stroke">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                        </div>
                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                            <g class="rating__fill">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                            </g>
                                            <g class="rating__stroke">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                            </g>
                                        </svg>
                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                            <div class="rating__fill">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                            <div class="rating__stroke">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                        </div>
                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                            <g class="rating__fill">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                            </g>
                                            <g class="rating__stroke">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                            </g>
                                        </svg>
                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                            <div class="rating__fill">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                            <div class="rating__stroke">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                        </div>
                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                            <g class="rating__fill">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                            </g>
                                            <g class="rating__stroke">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                            </g>
                                        </svg>
                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                            <div class="rating__fill">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                            <div class="rating__stroke">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                        </div>
                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                            <g class="rating__fill">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                            </g>
                                            <g class="rating__stroke">
                                                <use xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                            </g>
                                        </svg>
                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                            <div class="rating__fill">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                            <div class="rating__stroke">
                                                <div class="fake-svg-icon"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product__rating-legend">
                                <a class="link__feedbacks" href="#tabs__content">{{$product->feedbacks()->count()}} Отзывов</a><span>/</span><a href="#form__feedback" class="link__feedbacks">Написать отзыв</a>
                            </div>
                        </div>
                        <div class="product__description">
                            {{$product->short_description}}
                        </div>

                        <ul class="product__meta">
                            <li class="product__meta-availability">Наличие: <span class="text-success">{{$product->quantity}}</span></li>
                        </ul>
                    </div>
                    <!-- .product__info / end -->
                    <!-- .product__sidebar -->
                    <div class="product__sidebar">
                        <div class="product__availability">
                            Наличие: <span class="text-success">{{$product->quantity}}</span>
                        </div>
                        <div class="product__prices">
                            {{number_format($product->price,2,',',' ')}} ₽
                        </div>
                        <!-- .product__options -->
                        <form class="product__options form__add_to_cart" method="post" action="{{url('/add_to_cart')}}">
                            <input id="input__product_id" type="hidden" value="{{$product->id}}" name="product_id">
                            <div class="form-group product__option">
                                <label class="product__option-label" for="product-quantity">Кол-во</label>
                                <div class="product__actions">
                                    <div class="product__actions-item">
                                        <div class="input-number product__quantity">
                                            <input id="product-quantity" class="input-number__input form-control form-control-lg" type="number" min="1" value="1">
                                            <div class="input-number__add"></div>
                                            <div class="input-number__sub"></div>
                                        </div>
                                    </div>
                                    <div class="product__actions-item product__actions-item--addtocart">
                                        <button type="submit" class="btn btn-primary btn-lg">В корзину</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- .product__options / end -->
                    </div>
                    <!-- .product__end -->
                    <div class="product__footer">

                    </div>
                </div>
            </div>
            <div class="product-tabs  product-tabs--sticky">
                <div class="product-tabs__list">
                    <div class="product-tabs__list-body">
                        <div class="product-tabs__list-container container">
                            <a href="#tab-description" class="product-tabs__item product-tabs__item--active">Описание</a>
                            <a href="#tab-feedbacks" class="tab__feedbacks product-tabs__item">Отзывы ({{$product->feedbacks()->where('confirm',true)->count()}})</a>
                        </div>
                    </div>
                </div>
                <div class="product-tabs__content" id="tabs__content">
                    <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                        <div class="typography">
                            {!! $product->full_description !!}
                        </div>
                    </div>

                    @include('site.pages.product.feedbacks')

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('a.link__feedbacks').click(function(event){
            // event.preventDefault();
            $('a.tab__feedbacks').click();
        });

    </script>



    @if (session('status'))
        <script>
            alert("{{session('status')}}");
        </script>
    @endif
@endpush

@extends('site.layouts.app')

@section('content')

    <!-- .block-slideshow -->
    <div class="block-slideshow block-slideshow--layout--with-departments block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block"></div>
                <div class="col-12 col-lg-9">
                    <div class="block-slideshow__body">
                        <div class="owl-carousel">
                            <a class="block-slideshow__slide" href="{{url('/showcase/')}}">
                                <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop"
                                     style="background-image: url('{{asset("site/custom/images/programmer_mem.png")}}'); background-size: cover; background-position: center;"></div>
                                <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile"
                                     style="background-image: url('{{asset("site/custom/images/programmer_mem.png")}}'); background-size: cover; background-position: center;"></div>
                                <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">Я рассчитываю на Вас<br>
                                    </div>
                                    <div class="block-slideshow__slide-text">Я создал этот сайт, чтобы меня взяли на работу
                                    </div>
                                    <div class="block-slideshow__slide-button">
                                        <span class="btn btn-primary btn-lg">Перейти в каталог</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .block-slideshow / end -->
    <!-- .block-features -->
    <div class="block block-features block-features--layout--classic">
        <div class="container">
            <div class="block-features__list">
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="images/sprite.svg#fi-free-delivery-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">HTML+CSS</div>
                        <div class="block-features__subtitle">old newbie Junior</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="images/sprite.svg#fi-24-hours-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">JS</div>
                        <div class="block-features__subtitle">old newbie Junior</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="images/sprite.svg#fi-payment-security-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">PHP</div>
                        <div class="block-features__subtitle">old newbie Junior</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="images/sprite.svg#fi-tag-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">MySQL</div>
                        <div class="block-features__subtitle">old newbie Junior</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .block-features / end -->









@endsection

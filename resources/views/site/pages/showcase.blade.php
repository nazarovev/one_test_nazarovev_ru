@extends('site.layouts.app')

@section('content')
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{route('showcase')}}">Каталог</a>
                            @if(Illuminate\Support\Facades\Route::is('showcase_category'))
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                                </svg>
                            @else

                            @endif

                        </li>
                        @if(Illuminate\Support\Facades\Route::is('showcase_category'))
                            <li class="breadcrumb-item active" aria-current="page">{{$category->title}}</li>
                        @endif
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Каталог товаров
                    @if(Illuminate\Support\Facades\Route::is('showcase_category'))
                        - {{$category->title}}
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="block">
                    <div class="products-view">
                        <div class="products-view__options">
                            <div class="view-options view-options--offcanvas--always">
                                <div class="view-options__layout">
                                    <div class="layout-switcher">
                                        <div class="layout-switcher__list">
                                            <button data-layout="grid-4-full" data-with-features="false" title="Grid"
                                                    type="button"
                                                    class="layout-switcher__button  layout-switcher__button--active ">
                                                <svg width="16px" height="16px">
                                                    <use
                                                        xlink:href="{{asset('site/images/sprite.svg#layout-grid-16x16')}}"></use>
                                                </svg>
                                            </button>
                                            <button data-layout="list" data-with-features="false" title="List"
                                                    type="button"
                                                    class="layout-switcher__button ">
                                                <svg width="16px" height="16px">
                                                    <use
                                                        xlink:href="{{asset('site/images/sprite.svg#layout-list-16x16')}}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="view-options__legend">Показано {{count($products)}} товаров</div>
                                <div class="view-options__divider"></div>
                                <div class="view-options__control">
                                    <label for="">Порядок</label>
                                    <div>
                                        <select class="form-control form-control-sm" name="" id="">
                                            <option value="default">По умолчанию</option>
                                            <option value="title">Название (А-Я)</option>
                                            <option value="price_asc">Цена (сначала дешевле)</option>
                                            <option value="price_desc">Цена (сначала дороже)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="products-view__list products-list" data-layout="grid-4-full"
                             data-with-features="false"
                             data-mobile-grid-columns="2">
                            <div class="products-list__body">

                                @foreach($products as $product)
                                    <div class="products-list__item">
                                        <div class="product-card product-card--hidden-actions ">
                                            <div class="product-card__image product-image">
                                                <a href="{{route('product',['product_slug'=>$product->slug])}}"
                                                   class="product-image__body">
                                                    <img class="product-image__img"
                                                         src="{{asset('site/custom/images/products/'.$product->image)}}"
                                                         alt="">
                                                </a>
                                            </div>
                                            <div class="product-card__info">
                                                <div class="product-card__name">
                                                    <a href="{{route('product',['product_slug'=>$product->slug])}}">
                                                        {{$product->title}}</a>
                                                </div>
                                                <div class="product-card__rating">
                                                    <div class="product-card__rating-stars">
                                                        <div class="rating">
                                                            <div class="rating__body">
                                                                <svg class="rating__star rating__star--active"
                                                                     width="13px"
                                                                     height="12px">
                                                                    <g class="rating__fill">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div
                                                                    class="rating__star rating__star--only-edge rating__star--active">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                <svg class="rating__star rating__star--active"
                                                                     width="13px"
                                                                     height="12px">
                                                                    <g class="rating__fill">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div
                                                                    class="rating__star rating__star--only-edge rating__star--active">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                <svg class="rating__star rating__star--active"
                                                                     width="13px"
                                                                     height="12px">
                                                                    <g class="rating__fill">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div
                                                                    class="rating__star rating__star--only-edge rating__star--active">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                <svg class="rating__star rating__star--active"
                                                                     width="13px"
                                                                     height="12px">
                                                                    <g class="rating__fill">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div
                                                                    class="rating__star rating__star--only-edge rating__star--active">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                <svg class="rating__star " width="13px" height="12px">
                                                                    <g class="rating__fill">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal')}}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use
                                                                            xlink:href="{{asset('site/images/sprite.svg#star-normal-stroke')}}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div class="rating__star rating__star--only-edge ">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="product-card__rating-legend">{{$product->feedbacks()->count()}}
                                                        Отзывов
                                                    </div>
                                                </div>
                                                <p class="product-card__features-list">
                                                    {{$product->short_description}}
                                                </p>
                                            </div>
                                            <div class="product-card__actions">
                                                <div class="product-card__availability">
                                                    Наличие: <span class="text-success">{{$product->quantity}}</span>
                                                </div>
                                                <div class="product-card__prices">
                                                    {{number_format($product->price,2,',',' ')}} ₽
                                                </div>
                                                <div class="product-card__buttons">
                                                    <button data-product_id="{{$product->id}}" class="btn btn-primary btn-block product-card__addtocart"
                                                            type="button">
                                                        В корзину
                                                    </button>
                                                    <button data-product_id="{{$product->id}}"
                                                        class="btn btn-secondary btn-block product-card__addtocart product-card__addtocart--list"
                                                        type="button">В корзину
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
    <script>
        $('.product-card__addtocart').click(function(event){
            let product_id = $(this).data('product_id');
            let quantity = 1;

            // alert('hello');

            $.ajax({
                type: "POST",
                url: "{{route('add_to_cart')}}",
                data: {
                    _token: secure_token,
                    product_id: product_id,
                    quantity: quantity,
                },
                success: function (data) {
                    // alert(JSON.stringify(data));
                    location.reload();
                },
                dataType: "json",
            });

        });
    </script>
@endpush

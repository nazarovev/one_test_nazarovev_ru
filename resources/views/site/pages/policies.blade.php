@extends('site.layouts.app')

@section('content')
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Политика конфиденциальности</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Политика конфиденциальности
                    @if(Illuminate\Support\Facades\Route::is('showcase_category'))
                        - {{$category->title}}
                    @endif
                </h1>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col">
                <img src="{{asset('site/custom/images/policies.jpeg')}}" alt="">
            </div>
        </div>

    </div>
@endsection

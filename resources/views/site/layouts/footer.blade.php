<!-- site__footer -->
<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">Контакты</h5>
                            <div class="footer-contacts__text">
                                Звоните и пишите в любое время, буду рад общению на профессиональную тематику
                            </div>
                            <ul class="footer-contacts__contacts">
                                <li><i class="footer-contacts__icon far fa-envelope"></i> <a href="mailto:nazar.ev@ya.ru">nazar.ev@ya.ru</a></li>
                                <li><i class="footer-contacts__icon fas fa-mobile-alt"></i> <a href="tel:+79996868328">+7 999 68-68-328</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Информация</h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item"><a href="https://resume.nazarovev.ru" class="footer-links__link">Обо мне</a>
                                </li>
                                <li class="footer-links__item"><a href="{{url('/policies')}}" class="footer-links__link">Политика конфиденциальности</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</footer>
<!-- site__footer / end -->

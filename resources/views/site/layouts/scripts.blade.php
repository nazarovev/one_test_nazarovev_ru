<!-- js -->
<script src="{{asset('site/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('site/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('site/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('site/vendor/nouislider/nouislider.min.js')}}"></script>
<script src="{{asset('site/vendor/photoswipe/photoswipe.min.js')}}"></script>
<script src="{{asset('site/vendor/photoswipe/photoswipe-ui-default.min.js')}}"></script>
<script src="{{asset('site/vendor/select2/js/select2.min.js')}}"></script>
<script src="{{asset('site/js/number.js')}}"></script>
<script src="{{asset('site/js/main.js')}}"></script>
<script src="{{asset('site/js/header.js')}}"></script>
<script src="{{asset('site/vendor/svg4everybody/svg4everybody.min.js')}}"></script>
<script>
    svg4everybody();
</script>
<script type="text/javascript">
    var secure_token = '{{ csrf_token() }}';
</script>
<script>
    $('.dropcart__product-remove').click(function (event) {
        let product_id = $(this).data('product_id');

        if (confirm('Удалить товар из корзины?')) {

            $.ajax({
                type: "POST",
                url: "{{route('del_from_cart')}}",
                data: {
                    _token: secure_token,
                    product_id: product_id,
                },
                success: function (data) {
                    // alert(JSON.stringify(data));
                    location.reload();
                },
                dataType: "json",
            });
        }
    });

    $('.form__add_to_cart').submit(function (event) {
        event.preventDefault();
        let product_id = $('#input__product_id').val();
        let quantity = $('#product-quantity').val();

        // alert('hello');

        $.ajax({
            type: "POST",
            url: "{{route('add_to_cart')}}",
            data: {
                _token: secure_token,
                product_id: product_id,
                quantity: quantity,
            },
            success: function (data) {
                // alert(JSON.stringify(data));
                location.reload();
            },
            dataType: "json",
        });
    });
</script>

@stack('scripts')

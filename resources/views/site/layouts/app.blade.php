<!DOCTYPE html>
<html lang="ru" dir="ltr">

@include('site.layouts.head')

<body>
<!-- site -->
<div class="site">
    @include('site.layouts.mobile_header')
    @include('site.layouts.desktop_header')

    <!-- site__body -->
    <div class="site__body">
        @yield('content')
    </div>
    <!-- site__body / end -->
    @include('site.layouts.footer')
</div>
<!-- site / end -->
@include('site.layouts.quickview_modal')
@include('site.layouts.mobile_menu')

@include('site.layouts.scripts')

</body>

</html>

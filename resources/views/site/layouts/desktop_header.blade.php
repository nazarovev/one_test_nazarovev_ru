<!-- desktop site__header -->
<header class="site__header d-lg-block d-none">
    <div class="site-header">

        <div class="site-header__middle container">
            <div class="site-header__logo">
                <a href="{{url('/')}}">
                    <span style="font-size: 32px;">TEST SITE</span>
                </a>
            </div>
            <div class="site-header__search">
                <div class="search search--location--header ">
                    <div class="search__body">
                        <form class="search__form" action="/search">
                            <input class="search__input" name="search" placeholder="Простой поиск %LIKE%"
                                   aria-label="Site search" type="text" autocomplete="off">
                            <button class="search__button search__button--type--submit" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                            <div class="search__border"></div>
                        </form>
                        <div class="search__suggestions suggestions suggestions--location--header"></div>
                    </div>
                </div>
            </div>
            <div class="site-header__phone">
                <div class="site-header__phone-title">Евгений Назаров</div>
                <div class="site-header__phone-number">
                    <a href="tel:+79996868328">+7 (999) 68-68-328</a>
                </div>
            </div>
        </div>
        <div class="site-header__nav-panel">
            <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
            <div class="nav-panel nav-panel--sticky" data-sticky-mode="pullToShow">
                <div class="nav-panel__container container">
                    <div class="nav-panel__row">
                        <div class="nav-panel__departments">
                            <!-- .departments -->
                            <div
                                class="departments  @if(Illuminate\Support\Facades\Route::is('home')) departments--open departments--fixed @endif"
                                data-departments-fixed-by=".block-slideshow">
                                <div class="departments__body">
                                    <div class="departments__links-wrapper">
                                        <div class="departments__submenus-container"></div>
                                        <ul class="departments__links">
                                            @foreach($categories as $category)
                                                <li class="departments__item">
                                                    <a class="departments__item-link"
                                                       href="{{url('/showcase/'.$category->id)}}">
                                                        {{$category->title}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <button class="departments__button">
                                    <svg class="departments__button-icon" width="18px" height="14px">
                                        <use xlink:href="{{asset('site/images/sprite.svg#menu-18x14')}}"></use>
                                    </svg>
                                    Каталог товаров
                                    <svg class="departments__button-arrow" width="9px" height="6px">
                                        <use
                                            xlink:href="{{asset('site/images/sprite.svg#arrow-rounded-down-9x6')}}"></use>
                                    </svg>
                                </button>
                            </div>
                            <!-- .departments / end -->
                        </div>
                        <!-- .nav-links -->
                        <div class="nav-panel__nav-links nav-links">
                            <ul class="nav-links__list">
                                <li class="nav-links__item ">
                                    <a class="nav-links__item-link" href="{{url('/')}}">
                                        <div class="nav-links__item-body">
                                            Главная
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-links__item ">
                                    <a class="nav-links__item-link" href="{{url('/showcase')}}">
                                        <div class="nav-links__item-body">
                                            Каталог
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-links__item ">
                                    <a class="nav-links__item-link" target="_blank" href="https://resume.nazarovev.ru">
                                        <div class="nav-links__item-body">
                                            Обо мне
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <!-- .nav-links / end -->
                        <div class="nav-panel__indicators">

                            <div class="indicator indicator--trigger--click">
                                <a href="{{url('/cart')}}" class="indicator__button">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="{{asset('site/images/sprite.svg#cart-20')}}"></use>
                                                </svg>
                                                <span class="indicator__value">{{$cart['count']}}</span>
                                            </span>
                                </a>
                                <div class="indicator__dropdown">
                                    <!-- .dropcart -->
                                    <div class="dropcart dropcart--style--dropdown">
                                        <div class="dropcart__body">
                                            <div class="dropcart__products-list">
                                                @if(empty($cart['products']))
                                                    <div style="text-align: center; font-size: 18px;">
                                                        <span>ТУТ ПУСТО</span>
                                                    </div>
                                                @endif
                                                @foreach($cart['products'] as $item)
                                                    <div class="dropcart__product">
                                                        <div class="product-image dropcart__product-image">
                                                            <a href="{{route('product',['product_slug'=>$item['product']['slug']])}}"
                                                               class="product-image__body">
                                                                <img class="product-image__img"
                                                                     src="{{asset('site/custom/images/products/'.$item['product']['image'])}}"
                                                                     alt="">
                                                            </a>
                                                        </div>
                                                        <div class="dropcart__product-info">
                                                            <div class="dropcart__product-name">
                                                                <a href="{{route('product',['product_slug'=>$item['product']['slug']])}}">
                                                                    {{Str::limit($item['product']['title'],20)}}
                                                                </a></div>
                                                            <div class="dropcart__product-meta">
                                                                <span class="dropcart__product-quantity">{{$item['quantity']}}</span> ×
                                                                <span class="dropcart__product-price">{{$item['product']['price']}} Р</span>
                                                            </div>
                                                        </div>
                                                        <button type="button"
                                                                class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon"
                                                                data-product_id="{{$item['product']['id']}}"
                                                        >
                                                            <svg width="10px" height="10px">
                                                                <use
                                                                    xlink:href="{{asset('site/images/sprite.svg#cross-10')}}"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="dropcart__totals" style="padding-bottom: 16px;">
                                                <table>
                                                    <tr>
                                                        <th>Сумма:</th>
                                                        <td>{{number_format($cart['total'], 2)}} ₽</td>
                                                    </tr>

                                                </table>
                                            </div>
                                            @if(count($cart['products']) > 0)
                                                <div class="dropcart__buttons">
                                                    <a class="btn btn-secondary" href="{{url('/cart')}}">Перейти в корзину</a>
{{--                                                    <a class="btn btn-primary" href="{{url('/checkout')}}">Заказать</a>--}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- .dropcart / end -->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- desktop site__header / end -->

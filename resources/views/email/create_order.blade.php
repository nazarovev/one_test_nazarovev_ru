<p>Здравствуйте, вы создали заказ на сайте.</p>
@php($amount = 0)
@foreach($order->products as $product)
    @php($amount = $amount + ($product->pivot->quantity * $product->price))
    <p>{{Str::limit($product->title,20)}} - {{$product->pivot->quantity}} шт - {{ $product->price }} р. - {{ $product->pivot->quantity * $product->price }} р.</p>
@endforeach
<p></p>
<p>Сумма: {{$amount}}</p>


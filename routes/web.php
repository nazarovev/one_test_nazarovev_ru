<?php

use App\Http\Controllers\ActionController;
use App\Http\Controllers\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[FrontendController::class,'home'])->name('home');
Route::get('/showcase', [FrontendController::class,'showcase'])->name('showcase');
Route::get('/showcase/{category_id}', [FrontendController::class,'showcase_category'])->name('showcase_category');
Route::get('/cart', [FrontendController::class,'cart'])->name('cart');
Route::get('/product/{product_slug}', [FrontendController::class,'product'])->name('product');
Route::get('/policies', [FrontendController::class,'policies'])->name('policies');



Route::post('/product/{product_id}/create_feedback', [ActionController::class,'create_feedback'])->name('create_feedback');
Route::get('/product/{product_id}/confirm_feedback/{confirm_hash}', [ActionController::class,'confirm_feedback'])->name('confirm_feedback');

Route::post('/add_to_cart', [ActionController::class,'add_to_cart'])->name('add_to_cart');
Route::post('/del_from_cart', [ActionController::class,'del_from_cart'])->name('del_from_cart');
Route::post('/quantity_to_cart', [ActionController::class,'quantity_to_cart'])->name('quantity_to_cart');

Route::post('/checkout', [ActionController::class,'checkout'])->name('checkout');

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    public function images()
    {
        return $this->hasMany(
            ProductImage::class,
            'product_id',
            'id'
        )->orderBy('sort');
    }

    public function feedbacks()
    {
        return $this->hasMany(
            Feedback::class,
            'product_id',
            'id'
        )->orderBy('created_at');
    }

    public function category()
    {
        return $this->belongsTo(
            Category::class,
            'category_id',
            'id'
        );
    }

    public function orders()
    {
        return $this->belongsToMany(
            Order::class,
            'orders_products',
            'product_id',
            'order_id'
        )->withPivot(['price','quantity']);
    }

}

<?php

namespace App\Http\Controllers;

use App\Mail\MailCreateFeedback;
use App\Mail\MailCreateOrder;
use App\Models\Feedback;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ActionController extends Controller
{
    private $empty_cart;

    public function __construct()
    {
        $this->empty_cart = [
            'products' => [],
            'total' => 0,
            'count' => 0,
        ];
    }

    public function create_feedback($product_id, Request $request)
    {
        $product = Product::findOrFail($product_id);

        $validatedData = $request->validate([
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email'],
            'text' => ['required'],
            'rating' => ['required', 'numeric', 'min:1', 'max:5'],
        ]);

        $feedback = new Feedback();
        $feedback->name = $validatedData['name'];
        $feedback->avatar = 'avatar.jpg';
        $feedback->content = $validatedData['text'];
        $feedback->image = null;
        $feedback->images = null;
        $feedback->moderated = true;
        $feedback->email = $validatedData['email'];
        $feedback->confirm = false;
        $feedback->confirm_hash = Str::random(20);
        $feedback->product_id = $product_id;
        $feedback->rating = $validatedData['rating'];

        $feedback->save();

        Mail::to($feedback->email)
            ->send(new MailCreateFeedback(
                    $feedback->confirm_hash,
                    $product_id
                )
            );


        return back()->withInput()->with('status', 'Отзыв создан. Чтобы он был опубликован, подтвердите Email. Письмо уже направлено Вам на почту');
    }

    public function confirm_feedback($product_id, $confirm_hash)
    {
        $feedback = Feedback
            ::where('product_id', $product_id)
            ->where('confirm_hash', $confirm_hash)
            ->firstOrFail();

        $feedback->confirm = true;
        $feedback->confirm_hash = null;
        $feedback->save();

        return redirect(route('product', ['product_slug' => $feedback->product->slug]))->with('status', 'Отзыв был подтвержден и опубликован');
    }

    public function add_to_cart(Request $request)
    {
        $product_id = $request->get('product_id');
        $quantity = $request->get('quantity');

        $product = Product::findOrFail($product_id);

        $cart = session('cart', [
            'products' => [],
            'total' => 0,
            'count' => 0,
        ]);

        $find = 0;
        foreach ($cart['products'] as $key => $item) {
            if ($item['product']['id'] == $product->id) {
                $cart['products'][$key]['quantity'] = $cart['products'][$key]['quantity'] + $quantity;
                $find = 1;
                break;
            }
        }

        if ($find != 1) {
            $cart['products'][] = [
                'product' => $product,
                'quantity' => $quantity,
            ];
        }

        $cart['total'] = 0;
        $cart['count'] = 0;
        foreach ($cart['products'] as $item) {
            $cart['count'] = $cart['count'] + $item['quantity'];
            $amount = $item['quantity'] * $item['product']['price'];
            $cart['total'] = $cart['total'] + $amount;
        }

        session(['cart' => $cart]);

        return response()->json('ok');
    }

    public function del_from_cart(Request $request)
    {
        $product_id = $request->get('product_id');

        $cart = session('cart', [
            'products' => [],
            'total' => 0,
            'count' => 0,
        ]);

        $find = 0;
        foreach ($cart['products'] as $key => $item) {
            if ($item['product']['id'] == $product_id) {
                unset($cart['products'][$key]);
                $find = 1;
                break;
            }
        }

        $cart['total'] = 0;
        $cart['count'] = 0;
        foreach ($cart['products'] as $item) {
            $cart['count'] = $cart['count'] + $item['quantity'];
            $amount = $item['quantity'] * $item['product']['price'];
            $cart['total'] = $cart['total'] + $amount;
        }

        session(['cart' => $cart]);

        return response()->json('ok');
    }

    public function quantity_to_cart(Request $request)
    {
        $product_id = $request->get('product_id');
        $quantity = $request->get('quantity');

        $cart = session('cart', [
            'products' => [],
            'total' => 0,
            'count' => 0,
        ]);

        $find = 0;
        foreach ($cart['products'] as $key => $item) {
            if ($item['product']['id'] == $product_id) {
                $cart['products'][$key]['quantity'] = $quantity;
                $find = 1;
                break;
            }
        }

        $cart['total'] = 0;
        $cart['count'] = 0;
        foreach ($cart['products'] as $item) {
            $cart['count'] = $cart['count'] + $item['quantity'];
            $amount = $item['quantity'] * $item['product']['price'];
            $cart['total'] = $cart['total'] + $amount;
        }

        session(['cart' => $cart]);

        return response()->json('ok');
    }

    public function checkout(Request $request)
    {
        $cart = $request->get('cart');
        $name = $request->get('name');
        // $phone = $request->get('phone');
        $email = $request->get('email');

        if ($cart['count'] > 0) {
            $order = new Order();
            $order->buyer_name = $name;
            $order->buyer_phone = '+79999999999';
            $order->buyer_email = $email;
            $order->status = 'new';

            $order->save();

            foreach ($cart['products'] as $item) {
                $order->products()->attach([
                    $item['product']['id'] => [
                        'price' => $item['product']['price'],
                        'quantity' => $item['quantity'],
                    ]]);
            }

            Mail::to($order->buyer_email)
                ->send(new MailCreateOrder(
                        $order,
                    )
                );

            $cart = $this->empty_cart;
            session(['cart'=>$cart]);

            return 'ok';
        }

        return 'fail';


    }
}

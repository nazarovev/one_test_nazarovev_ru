<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    private $empty_cart;

    public function __construct()
    {
        $this->empty_cart = [
            'products' => [],
            'total' => 0,
            'count' => 0,
        ];
    }
    public function home()
    {
        return view('site.pages.home', [
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

    public function showcase()
    {
        return view('site.pages.showcase', [
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'products' => \App\Models\Product::all(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

    public function showcase_category($category_id)
    {
        return view('site.pages.showcase', [
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'category' => \App\Models\Category::findOrFail($category_id),
            'products' => \App\Models\Product::where('category_id',$category_id)->get(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

    public function product($product_slug)
    {
        $cart = session('cart',['products' => [], 'total' => 0]);
        // dd($cart);
        return view('site.pages.product', [
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'product' => \App\Models\Product::where('slug',$product_slug)->firstOrFail(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

    public function cart()
    {
        return view('site.pages.cart', [
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

    public function policies()
    {
        return view('site.pages.policies',[
            'categories' => \App\Models\Category::orderBy('sort')->get(),
            'cart' => session('cart',$this->empty_cart),
        ]);
    }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name'); // Имя автора
            $table->string('avatar')->nullable(); // Аватар автора
            $table->text('content'); // Содержание отзыва
            $table->text('image')->nullable(); // Главное изображение отзыва
            $table->text('images')->nullable(); // Остальные изображения отзыва (JSON)

            $table->boolean('moderated')->default(true); // Флаг модерации

            $table->string('email'); // Почта для подтверждения публикации отзыва
            $table->boolean('confirm')->default(false); // Флаг подтверждения отзыва
            $table->string('confirm_hash')->nullable(); // Хеш подтверждения отзыва

            $table->foreignId('product_id'); // Ссылка на товар
            $table->foreign('product_id')->references('id')->on('products');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feedbacks');
    }
};

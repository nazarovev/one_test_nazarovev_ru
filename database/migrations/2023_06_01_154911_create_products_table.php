<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('slug')->unique(); //URL-товара
            $table->string('title'); // Название
            $table->text('short_description')->nullable(); // Короткое описание
            $table->text('full_description')->nullable(); // Полное описание
            $table->decimal('price',15,2)->nullable(); // Цена
            $table->integer('quantity',)->default(0); // Кол-во
            $table->string('image')->nullable(); // Главное изображение

            $table->foreignId('category_id'); // Ссылка на категорию
            $table->foreign('category_id')->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
